#ifndef __HANDLE_IPV4_PACKET_FUNCTION
#define __HANDLE_IPV4_PACKET_FUNCTION

#include <sys/types.h> // ssize_t

void handle_ipv4_packet(unsigned char*, ssize_t);

#endif
