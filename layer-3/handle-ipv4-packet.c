#include <stdbool.h> // bool / true / false
#include <stdint.h> // uint*_t
#include <stdio.h> // printf()

#include "helper.h" // print_hex()

#include "layer-4/all.h"

extern char *yes_no[2];

// SInce this is a very old protocol with many updates, I recomment starting with the wikipedia page
// https://en.wikipedia.org/wiki/Internet_Protocol_version_4
// 
// https://datatracker.ietf.org/doc/html/rfc791
void handle_ipv4_packet(unsigned char* ipv4_packet, ssize_t ipv4_packet_length){
	uint8_t protocol_version = (ipv4_packet[0] & 0xF0) >> 4;
	uint8_t ihl = ipv4_packet[0] & 0x0F;
	ssize_t data_offset = ihl << 2;

	// https://datatracker.ietf.org/doc/html/rfc2474
	uint8_t dscp = ipv4_packet[1] & 0xFC;

	// https://datatracker.ietf.org/doc/html/rfc3168
	uint8_t ecn = ipv4_packet[1] & 0x03;
	bool ect0 = ecn & 0x01;
	bool ect1 = ecn & 0x02;


	uint16_t total_length = (ipv4_packet[2] << 8) | ipv4_packet[3];

	// https://datatracker.ietf.org/doc/html/rfc6864
	uint16_t identification = (ipv4_packet[4] << 8) | ipv4_packet[5];

	uint8_t flags = ipv4_packet[6] & 0x07;
	uint8_t fragment_offset = ((ipv4_packet[6] & 0xf8) << 8) | ipv4_packet[7];

	bool flag_dont_fragment = flags & 0x02;
	bool flag_more_fragments = flags & 0x04;

	uint8_t time_to_live = ipv4_packet[8];

	// https://datatracker.ietf.org/doc/html/rfc790
	// which eventually gets to
	// https://datatracker.ietf.org/doc/html/rfc3232
	// which says the protocol numbers may be found at
	// https://www.iana.org/assignments/protocol-numbers/protocol-numbers.xhtml
	uint8_t protocol = ipv4_packet[9];

	uint16_t header_checksum = (ipv4_packet[10] << 8) | ipv4_packet[11];

	// Now print information from the IPv4 packet
	printf("IPv4 Packet - %zi\n", ipv4_packet_length);

	print_hex(ipv4_packet, data_offset, 0);

	printf(
		"\tSource IP Address:\t%u.%u.%u.%u\n"
		"\tDestination IP Address:\t%u.%u.%u.%u\n"
		"\n",
		ipv4_packet[12],
		ipv4_packet[13],
		ipv4_packet[14],
		ipv4_packet[15],

		ipv4_packet[16],
		ipv4_packet[17],
		ipv4_packet[18],
		ipv4_packet[19]
	);

	printf(
		"\tProtocol Version:\t%u\n"
		"\tInternet Header Length:\t%u\n"
		"\tDSCP:\t\t\t%u\n"
		"\n",
		protocol_version,
		ihl,
		dscp
	);

	printf(
		"\tECN %02X:\n"
		"\t\tECT(0) %s\n"
		"\t\tECT(1) %s\n"
		"\t\tCE %s\n"
		"\n",
		ecn,
		yes_no[ect0],
		yes_no[ect1],
		yes_no[ect0 && ect1]
	);

	printf(
		"\tTotal Length:\t%u\n"
		"\tIdentification:\t%04X\n"
		"\n",
		total_length,
		identification
	);

	printf(
		"\tFlags %X:\n"
		"\t\tReserved (must be 0) %u\n"
		"\t\tDon't Fragment %s\n"
		"\t\tMore Fragments %s\n"
		"\n",
		flags,
		flags & 0x01,
		yes_no[flag_dont_fragment],
		yes_no[flag_more_fragments]
	);

	printf(
		"\tFragment Offset:\t%u\n"
		"\tTime To Live:\t\t%u\n"
		"\tProtocol:\t\t%u (https://www.iana.org/assignments/protocol-numbers/protocol-numbers.xhtml)\n"
		"\tHeader Checksum: %04X\n"
		"\n",
		fragment_offset,
		time_to_live,
		protocol,
		header_checksum
	);

	switch(protocol){
		case 6:
			handle_tcp_segment(&ipv4_packet[data_offset], ipv4_packet_length - data_offset);
		break;

		case 17:
			handle_udp_datagram(&ipv4_packet[data_offset], ipv4_packet_length - data_offset);
		break;
	}
}
