#ifndef __HANDLE_UDP_DATAGRAM_FUNCTION
#define __HANDLE_UDP_DATAGRAM_FUNCTION

#include <sys/types.h> // ssize_t

void handle_udp_datagram(unsigned char*, ssize_t);

#endif
