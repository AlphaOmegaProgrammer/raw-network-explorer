#ifndef __HANDLE_TCP_SEGMENT_FUNCTION
#define __HANDLE_TCP_SEGMENT_FUNCTION

#include <sys/types.h> // ssize_t

void handle_tcp_segment(unsigned char*, ssize_t);

#endif
