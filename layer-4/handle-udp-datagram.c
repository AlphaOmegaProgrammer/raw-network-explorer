#include <stdbool.h> // bool / true / false
#include <stdint.h> // uint8_t / uint16_t / uint32_t
#include <sys/types.h> // ssize_t
#include <stdio.h> // printf

#include "helper.h" // print_hex()

void handle_udp_datagram(unsigned char *udp_datagram, ssize_t udp_datagram_length){
	uint16_t source_port = (udp_datagram[0] << 8) | udp_datagram[1];
	uint16_t destination_port = (udp_datagram[2] << 8) | udp_datagram[3];

	uint16_t length = (udp_datagram[4] << 8) | udp_datagram[5];
	uint16_t checksum = (udp_datagram[6] << 8) | udp_datagram[7];


	printf("UDP Datagram - %zi\n", udp_datagram_length);

	print_hex(udp_datagram, 8, 0);

	printf(
		"\tSource Port\t\t%u\n"
		"\tDestination Port\t%u\n"
		"\n",
		source_port,
		destination_port
	);

	printf(
		"\tLength\t\t%u\n"
		"\tChecksum\t%04X\n"
		"\n",
		length,
		checksum
	);

	if(udp_datagram_length == 8)
		printf("(no data)\n");
	else
		print_hex(&udp_datagram[8], udp_datagram_length - 8, 8);
}
