#include <stdbool.h> // bool / true / false
#include <stdint.h> // uint8_t / uint16_t / uint32_t
#include <sys/types.h> // ssize_t
#include <stdio.h> // printf

#include "helper.h" // print_hex()

extern char *yes_no[2];

void handle_tcp_segment(unsigned char *tcp_segment, ssize_t tcp_segment_length){
	uint16_t source_port = (tcp_segment[0] << 8) | tcp_segment[1];
	uint16_t destination_port = (tcp_segment[2] << 8) | tcp_segment[3];

	uint32_t sequence_number = (tcp_segment[4] << 24) | (tcp_segment[5] << 16) | (tcp_segment[6] << 8) | tcp_segment[7];
	uint32_t acknowledgement_number = (tcp_segment[8] << 24) | (tcp_segment[9] << 16) | (tcp_segment[10] << 8) | tcp_segment[11];

	uint8_t data_offset = tcp_segment[12] >> 4;

	uint8_t flags = tcp_segment[13];

	bool flag_congestion_window_reduced = flags & 0x80;
	bool flag_ecn_echo = flags & 0x40;
	bool flag_urgent = flags & 0x20;
	bool flag_acknowledgement = flags & 0x10;
	bool flag_push = flags & 0x08;
	bool flag_reset = flags & 0x04;
	bool flag_syn = flags & 0x02;
	bool flag_fin = flags & 0x01;

	uint16_t window_size = (tcp_segment[14] << 8) | tcp_segment[15];
	uint16_t checksum = (tcp_segment[16] << 8) | tcp_segment[17];
	uint16_t urgent_pointer = (tcp_segment[18] << 8) | tcp_segment[19];

	printf("TCP Segment - %zi\n", tcp_segment_length);

	printf(
		"\tSource Port\t\t%u\n"
		"\tDestination Port\t%u\n"
		"\n",
		source_port,
		destination_port
	);

	printf(
		"\tSequence Number\t\t%08X\n"
		"\tAcknowledgement Number\t%08X\n"
		"\tData Offset\t\t%u\n"
		"\n",
		sequence_number,
		acknowledgement_number,
		data_offset
	);

	printf(
		"\tFlags: %02X\n"
		"\t\tCongestion Window Reduced %s\n"
		"\t\tECN Echo %s\n"
		"\t\tUrgent %s\n"
		"\t\tAcknowledgement %s\n"
		"\t\tPush %s\n"
		"\t\tReset %s\n"
		"\t\tSynchronize Sequence Numbers %s\n"
		"\t\tFIN %s\n"
		"\n",
		flags,
		yes_no[flag_congestion_window_reduced],
		yes_no[flag_ecn_echo],
		yes_no[flag_urgent],
		yes_no[flag_acknowledgement],
		yes_no[flag_push],
		yes_no[flag_reset],
		yes_no[flag_syn],
		yes_no[flag_fin]
	);

	printf(
		"\tWindow Size\t%u\n"
		"\tChecksum\t%04X\n"
		"\tUrgent Pointer\t%u\n"
		"\n",
		window_size,
		checksum,
		urgent_pointer
	);

	ssize_t data_index = data_offset << 2;

	if(data_index == tcp_segment_length)
		printf("(no data)\n");
	else
		print_hex(&tcp_segment[data_index], tcp_segment_length - data_index, data_index);
}
