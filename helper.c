#include <stdio.h> // printf
#include <sys/types.h> // ssize_t

char *yes_no[2] = {
	"No",
	"Yes"
};

void print_hex(unsigned char *buffer, ssize_t length_to_print, ssize_t line_byte_number){
	unsigned char hex_buffer[8];
	unsigned char chr_buffer[8];

	ssize_t buffer_index = 0;
	unsigned char starting_loop_offset = line_byte_number % 8;
	line_byte_number -= starting_loop_offset;
	
	while(buffer_index < length_to_print){
		unsigned char loop_index = 0;

		for(; loop_index < starting_loop_offset; loop_index++){
			hex_buffer[loop_index] = 0;
			chr_buffer[loop_index] = '.';
		}

		for(; loop_index < 8 && buffer_index < length_to_print; loop_index++, buffer_index++){
			hex_buffer[loop_index] = buffer[buffer_index];

			if(buffer[buffer_index] < ' ' || buffer[buffer_index] > '~')
				chr_buffer[loop_index] = '.';
			else
				chr_buffer[loop_index] = buffer[buffer_index];
		}

		for(; loop_index < 8; loop_index++){
			hex_buffer[loop_index] = 0;
			chr_buffer[loop_index] = '.';
		}

		printf(
			"%04zi   %02X %02X %02X %02X %02X %02X %02X %02X   %c%c%c%c%c%c%c%c\n",
			line_byte_number,

			hex_buffer[0],
			hex_buffer[1],
			hex_buffer[2],
			hex_buffer[3],
			hex_buffer[4],
			hex_buffer[5],
			hex_buffer[6],
			hex_buffer[7],

			chr_buffer[0],
			chr_buffer[1],
			chr_buffer[2],
			chr_buffer[3],
			chr_buffer[4],
			chr_buffer[5],
			chr_buffer[6],
			chr_buffer[7]
		);

		line_byte_number += 8;
		starting_loop_offset = 0;
	}

	printf("\n");
}
