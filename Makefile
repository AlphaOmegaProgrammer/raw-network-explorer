TARGET = raw_network_explorer

OBJECTS = \
	main.o \
	helper.o \
	handle-ethernet-frame.o \
	layer-3/handle-ipv4-packet.o \
	layer-4/handle-tcp-segment.o \
	layer-4/handle-udp-datagram.o \


.PHONY: all clean

all: $(TARGET)

clean:
	-$(RM) $(TARGET) $(OBJECTS)



$(TARGET): $(OBJECTS)
	$(CC) -o $(TARGET) $(OBJECTS)

%.o: %.c
	$(CC) $(CFLAGS) -I . -I layer-3 -I layer-4 -c $< -o $@
	
