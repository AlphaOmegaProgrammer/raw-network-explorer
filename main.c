#include <arpa/inet.h> // htons()
#include <linux/if_ether.h> // ETH_P_ALL
#include <stdio.h> // printf()
#include <sys/socket.h> // socket()
#include <sys/types.h> // ssize_t
#include <unistd.h> // read()

#include "handle-ethernet-frame.h" // handle_ethernet_frame()

int main(void){
	// Declare a read buffer
	unsigned char read_buffer[1000000];

	// Open the raw socket for all protocols
	int protocol_type = htons(ETH_P_ALL);
	int socket_fd = socket(AF_PACKET, SOCK_RAW, protocol_type);

	for(;;){
		ssize_t read_result = read(socket_fd, read_buffer, 1000000);
		// If reading from the raw socket failed, break the loop
		if(read_result < 0)
			break;

		// Firstly, the read buffer will contain a ayer 2 Ethernet frame
		// https://en.wikipedia.org/wiki/Ethernet_frame#Structure
		if(read_result < 22){
			// Network packet too small to contain ethernet frame header... Ignoring
			continue;
		}

		handle_ethernet_frame(read_buffer, read_result);

		printf("\n\n");
	}

	// If the loop was broken out of, read had some kind of error
	perror("Reading from raw socket failed:\n");
	printf("\nExiting...\n");

	return 0;
}
