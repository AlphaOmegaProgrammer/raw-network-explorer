#ifndef __HANDLE_ETHERNET_FRAME_FUNCTION
#define __HANDLE_ETHERNET_FRAME_FUNCTION

#include <sys/types.h> // ssize_t

void handle_ethernet_frame(unsigned char*, ssize_t);

#endif
