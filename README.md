# Raw Network Explorer

The code in meant to be an educational, exploritory tool for working with raw layer 2 sockets. In this repository, the socket gets traffic from all protocols.

This is a tool I built for myself to learn and explore with, so don't expect too much testing, support, optimization, etc. I decided to publish this code in case anybody else wanted to explore raw layer 2 sockets, or in case anybody wanted to add support for more protocols.


## Building

This can probably be ported to other platforms quite easily (at a glance, it appears that even Windows supports raw layer 2 sockets). However, I will assume this only works on Linux.

To build, simply `cd` into the directory that results from cloning this repository and run `make`. This will produce an executable called `raw_network_explorer`. Simply run the executable and either wait for or perform some network requests to start seeing output.


## Learning

I've written this code and output in a way such that it should be easy to research and learn about the data that is being parsed from the network data.

This is a layer 2 socket, meaning it operates on layer 2 of the [OSI model](https://en.wikipedia.org/wiki/OSI_model#Layer_architecture). For the purposes of this code, it is likely that only layers 2, 3, and 4 will be interfaced with.


## Protocols

The following is a list of protocols that are currently recognized, parsed, and displayed by this repository

### Layer 2
 * Ethernet - https://en.wikipedia.org/wiki/Ethernet

### Layer 3
 * IPv4 - https://datatracker.ietf.org/doc/html/rfc791

### Layer 4
 * TCP - https://datatracker.ietf.org/doc/html/rfc9293
 * UDP - https://datatracker.ietf.org/doc/html/rfc768
