#include <stdbool.h> // bool / true / false
#include <stdint.h> // uint16_t / uint32_T
#include <stdio.h> // printf
#include <sys/types.h> // ssize_t

#include "helper.h" // print_hex()

#include "handle-ipv4-packet.h" // handle_ipv4_packet()

extern char *yes_no[2];

// https://en.wikipedia.org/wiki/Ethernet_frame#Structure
void handle_ethernet_frame(unsigned char *ethernet_frame, ssize_t ethernet_frame_length){
	// https://en.wikipedia.org/wiki/IEEE_802.1Q
	// https://en.wikipedia.org/wiki/IEEE_802.1ad
	uint32_t _802_1_Q_tags_count = 0;
	uint32_t _802_1_ad_tags_count = 0;
	uint32_t payload_length_or_ethertype_offset = 0;

	for(;;){
		// Count and skip 802.1Q tags
		if(ethernet_frame[12 + payload_length_or_ethertype_offset] == 0x81 && ethernet_frame[13 + payload_length_or_ethertype_offset] == 0){
			payload_length_or_ethertype_offset += 4;
			_802_1_Q_tags_count++;
			continue;
		}

		// Count and skip 802.1ad tags
		if(ethernet_frame[12 + payload_length_or_ethertype_offset] == 0x88 && ethernet_frame[13 + payload_length_or_ethertype_offset] == 0xA8){
			payload_length_or_ethertype_offset += 4;
			_802_1_ad_tags_count++;
			continue;
		}

		break;
	}


	// https://en.wikipedia.org/wiki/EtherType
	uint16_t payload_length_or_ethertype = (ethernet_frame[12 + payload_length_or_ethertype_offset] << 8)
		| ethernet_frame[13 + payload_length_or_ethertype_offset]
	;

	// It's likely that the ethernet frame will have an Ethertype
	bool has_ethertype = payload_length_or_ethertype >= 0x0600;
	bool payload_length_valid = has_ethertype || payload_length_or_ethertype >= 46;


	// https://en.wikipedia.org/wiki/Jumbo_frame
	bool is_jumbo_frame = !has_ethertype && (payload_length_or_ethertype > 1500);


	uint32_t payload_check_sequence = (ethernet_frame[ethernet_frame_length - 3] << 24)
		| (ethernet_frame[ethernet_frame_length - 2] << 16)
		| (ethernet_frame[ethernet_frame_length - 1] << 8)
		| ethernet_frame[ethernet_frame_length]
	;

	// Now print the ethernet frame information
	printf("Ethernet Frame - %zi bytes\n", ethernet_frame_length);

	print_hex(ethernet_frame, 14 + payload_length_or_ethertype_offset, 0);
	printf("...Other data...\n\n");
	print_hex(&ethernet_frame[ethernet_frame_length - 4], 4, ethernet_frame_length - 4);



	// Print the destination and source MAC addresses
	printf(
		"\tSource MAC:\t\t%02X:%02X:%02X:%02X:%02X:%02X\n"
		"\tDestination MAC:\t%02X:%02X:%02X:%02X:%02X:%02X\n"
		"\n",

		ethernet_frame[6],
		ethernet_frame[7],
		ethernet_frame[8],
		ethernet_frame[9],
		ethernet_frame[10],
		ethernet_frame[11],

		ethernet_frame[0],
		ethernet_frame[1],
		ethernet_frame[2],
		ethernet_frame[3],
		ethernet_frame[4],
		ethernet_frame[5]
	);

	// Print counts of 802.1Q and 802.1ad tags
	printf(
		"\t802.1Q tags count:\t%u\n"
		"\t802.1ad tags count:\t%u\n"
		"\n",
		_802_1_Q_tags_count,
		_802_1_ad_tags_count
	);

	// Print whether or not the ethernet frame has an ethertype
	printf("\tHas Ethertype? %s\n", yes_no[has_ethertype]);

	if(has_ethertype){
		printf("\tEthertype: 0x%04X (Lookup on https://en.wikipedia.org/wiki/EtherType#Values)\n\n", payload_length_or_ethertype);
	}else{
		printf("\tPayload Length: %u\n\n", payload_length_or_ethertype);
	}


	// Print the CRC checksum
	printf("\tCRC Checksum:\t\t%08X\n\n", payload_check_sequence);


	// Now print other miscillaneous information
	printf("\tPayload at least 46 bytes? %s\n"
		"\tJumbo frame (over 1500 bytes)? %s\n"
		"\n",
		yes_no[payload_length_valid],
		yes_no[is_jumbo_frame]
	);


	// If the ethernet frame contains an IPv4 packet, handle the IPv4 packet
	if(has_ethertype && payload_length_or_ethertype == 0x0800)
		handle_ipv4_packet(&ethernet_frame[14 + payload_length_or_ethertype_offset], ethernet_frame_length - 14 - payload_length_or_ethertype_offset);
}
