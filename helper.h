#ifndef __HELPER_FUNCTIONS
#define __HELPER_FUNCTION

#include <sys/types.h> // ssize_t

void print_hex(unsigned char*, ssize_t, ssize_t);

#endif
